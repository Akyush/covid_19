import { Body, Header, Title } from 'native-base';
import React, { Component } from 'react';
import { Button, View, ScrollView, RefreshControl } from 'react-native';
import WebView from 'react-native-webview';

export default class HomeScreen extends Component {
    constructor() {
        super();
        this.state = {
            isworld: false,
            isIndia: false,
            isFullScreen: false,
            isZee: false,
            isSab: false,
            isDhar: false,
            refreshing: false,
        }
    }

    onRefresh = () => {
        this.setState({ refreshing: true });
        fetch("https://www.covid19india.org/").then(() => {
            this.setState({ refreshing: false });
        });
    }

    world = () => {
        console.log('world')
        this.setState({ isworld: true, isIndia: false, isZee: false })

    }
    India = () => {
        console.log('india')
        this.setState({ isworld: false, isIndia: true, isZee: false, isSab: false, isDhar: false })
    }
    Zee = () => {
        this.setState({ isZee: true, isworld: false, isIndia: false, isSab: false, isDhar: false })
    }
    Sab = () => {
        this.setState({ isZee: false, isworld: false, isIndia: false, isSab: true, isDhar: false })
    }
    Dhar = () => {
        this.setState({ isZee: false, isworld: false, isIndia: false, isSab: false, isDhar: true })
    }
    render() {
        const { isworld, isIndia, isZee, isSab, isDhar } = this.state;
        var Dhar = "https://www.bhaskar.com/local/mp/indore/news/coronavirus-positive-patient-found-in-mp-dhar-bakhtawar-marg-madhya-pradesh-cases-rise-to-341-127137562.html";
        var India = 'https://www.covid19india.org/';
        var world = 'https://www.worldometers.info/coronavirus/?';
        var zeenews = "https://zeenews.india.com/live-tv";
        var Sab = "https://www.sonyliv.com/details/show/4600324971001/Taarak-Mehta-Ka-Ooltah-Chashmah";
        return (
            <View style={{ flex: 1 }} >


                <Header>
                    <Body>
                        <Title style={{ justifyContent: 'center', alignSelf: 'center' }}>Covid 19</Title>
                    </Body>
                </Header>


                {isIndia === true || isZee === true || isSab === true || isDhar === true ? null : isworld ?
                    <View style={{ margin: 5, padding: 5 }}>
                        <Button title="Back" onPress={() => this.setState({ isIndia: false, isworld: false })} />
                    </View> : <View style={{ margin: 5, padding: 5 }}>
                        <Button title="Covid 19 World Statistics" onPress={() => this.world()} />
                    </View>}
                {isworld && <WebView source={{ uri: world }} style={{ marginTop: 10 }} />}
                {isworld === true || isZee === true || isSab === true || isDhar === true ? null : isIndia ?
                    <View style={{ margin: 5, padding: 5 }}>
                        <Button title="Back" onPress={() => this.setState({ isIndia: false, isworld: false })} />
                    </View> : <View style={{ margin: 5, padding: 5 }}>
                        <Button title="Covid 19 India Statistics" onPress={() => this.India()} />
                    </View>}

                {isIndia && <WebView source={{ uri: India }} style={{ marginTop: 10 }} />}

                {isIndia === true || isworld === true || isSab === true || isDhar === true ? null : isZee ?
                    <View style={{ margin: 5, padding: 5 }}>
                        <Button title="Back" onPress={() => this.setState({ isIndia: false, isworld: false, isZee: false })} />
                    </View> : <View style={{ margin: 5, padding: 5 }}>
                        <Button title="Zee Live News" onPress={() => this.Zee()} />
                    </View>}

                {isZee && <WebView style={[this.state.isFullScreen ? { width: '100%', height: Dimensions.get('window').width / 1 } : { width: '100%', height: 230, resizeMode: 'contain' }, { backgroundColor: 'black' }]}
                    source={{ uri: zeenews }} />}

                {isIndia === true || isZee === true || isworld === true || isDhar === true ? null : isSab ?
                    <View style={{ margin: 5, padding: 5 }}>
                        <Button title="Back" onPress={() => this.setState({ isIndia: false, isworld: false, isZee: false, isSab: false })} />
                    </View> : <View style={{ margin: 5, padding: 5 }}>
                        <Button title="Sab TV Live" onPress={() => this.Sab()} />
                    </View>}

                {isSab && <WebView style={[this.state.isFullScreen ? { width: '100%', height: Dimensions.get('window').width / 1 } : { width: '100%', height: 230, resizeMode: 'contain' }, { backgroundColor: 'black' }]}
                    source={{ uri: Sab }} />}

                {isIndia === true || isZee === true || isworld === true || isSab === true ? null : isDhar ?
                    <View style={{ margin: 5, padding: 5 }}>
                        <Button title="Back" onPress={() => this.setState({ isIndia: false, isworld: false, isZee: false, isSab: false, isDhar: false })} />
                    </View> : <View style={{ margin: 5, padding: 5 }}>
                        <Button title="Dhar News" onPress={() => this.Dhar()} />
                    </View>}

                {isDhar && <WebView style={[this.state.isFullScreen ? { width: '100%', height: Dimensions.get('window').width / 1 } : { width: '100%', height: 230, resizeMode: 'contain' }, { backgroundColor: 'black' }]}
                    source={{ uri: Dhar }} />}


            </View>
        )
    };
};